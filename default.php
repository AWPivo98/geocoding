<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Geocoding</title>
	<style>
		.row{
			padding-bottom: 1em;
		}
		.danger{
			border-color: red;
		}
	</style>
  </head>
  <body>
	<div class="container">
		<h1>Geocoding<h1>
		<h4>Validazione dati inseriti</h4>
		<div class="row">
			<div class="col-sm-3">
				<span>Nazione *</span>
			</div>
			<div class="col-sm-9">
				<select class="form-control obbligatorio" id="state">
					<option value=""> -- Selezionare uno Stato --</option>
					<option value="italy">Italia</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Nome *</span>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control obbligatorio">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Cognome *</span>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control obbligatorio">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>eMail *</span>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control obbligatorio">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Presso</span>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Telefono *</span>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control obbligatorio">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Indirizzo *</span>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control obbligatorio" id="street">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Indirizzo (Linea 2)</span>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>CAP / ZIP Code *</span>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control obbligatorio" id="postalcode">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Citt&agrave; *</span>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control obbligatorio" id="city">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Provincia/Stato *</span>
			</div>
			<div class="col-sm-9">
				<select class="form-control obbligatorio" id="county">
					<option value=""> -- Selezionare la Provincia -- </option>
					<option value="CN">Cuneo</option>
					<option value="AL">Alessandria</option>
					<option value="GE">Genova</option>
					<option value="SV">Savona</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Indicazioni per il Corriere</span>
			</div>
			<div class="col-sm-9">
				<textarea class="form-control">
				
				</textarea>
			</div>
		</div>
		<div class="row">
			<button type="button" class="btn btn-dark" onclick="ControllaDati()">Controlla Dati</button>
		</div>
		
		
		<div class="row" id="ResponseDiv" style="display: none">
			<div class="col-sm-3">
				<span>Responso:</span>
			</div>
			<div class="col-sm-9">
				<pre id="response"></pre>
			</div>
		</div>
	</div>
    


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	
	
	<script>
		function ControllaDati()
		{
			var error = 0;	// -- Variabile Conteggio campi vuoti
			
			// -- Controllo Dati Obbligatori
			$(".obbligatorio").each(function() {
				if($(this).val() == 0 || $(this).val() == null || $(this).val() == "")
				{
					$(this).addClass("danger");
					error++;
				}
				else
					$(this).removeClass( "danger" );
			});
			
			if(error == 0)
			{
				callAPI();
			}
			else
			{
				$("#ResponseDiv").hide();
			}
		}
		
		function callAPI()
		{
			$.ajax({
				url: 'callApi.php',
				type: 'post',
				data: {
					"street": 		$("#street").val(),
					"city": 		$("#city").val(),
					"county": 		$("#county").val(),
					"state": 		$("#state").val(),
					"postalcode": 	$("#postalcode").val()
				},
				success: function( data, textStatus, jQxhr ){
					$("#ResponseDiv").show();
					$('#response').text(data);
				}
			});
		}
	</script>
  </body>
</html>